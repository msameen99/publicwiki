created: 20180310170857233
modified: 20190309004544044
tags: Journal
title: 10th-March-2018: DissonanceOfFocus
type: text/vnd.tiddlywiki

Difficulty with understanding the whole thing of the efficiency decision and its importance amidst wage rate changes due to a dissonance of focus when the wage rates increase.

Efficiency decision's equation: (MPl/w = MPk/r), or marginal product of labor over the wage rate //ought to (according to theory)// equal the marginal product of capital (K) over the rental rate [of capital].

I suggest that I'm getting confused by a dissonance of focus because of the following:

Wage rates increase, leading the ratio of MPl to w (MPl/w) to shrink [w UP, but the ratio as a whole DOWN, as MPl remains constant], and this changes the efficiency decision's equation, making it unequal, as the change in the wage rate (w) does not necessarily change the values on the right hand side of the equation.

The equation thus becomes: (MPl/w < MPk/r)

According to the logic of the efficiency decision (rule?), one should reinvest money currently used for labor into capital, as it's more efficient to use one's money there, as the ratio (MPk/r) is now GREATER THAN (MPl/w).

Logically, this makes sense, but when I'm thinking quickly about these matters I get confused. I suspect that it is because firstly I am focusing upon the wage, and I am focusing upon it either too much or in the wrong way.

"w is going up! Oh no!" I think first. "The equation is changed, and now it is unequal!" I think second. But thirdly, my mind is muddied by the relation between the change in the wage and the direction of the inequality; "wage is up, so shouldn't the inequality say that the side with the wage is GREATER THAN what it was before?"

The matter is made worse, I suspect, when the resulting inequality is given to me. "Ah, there's the wage, and there's the inequality sign! LESS THAN! Wages have gone down, BUY BUY BUY"