created: 20180110231622412
modified: 20190309004544049
tags: TiddlyWikiTweaks Journal
title: 10th January 2018
type: text/vnd.tiddlywiki

Realized last night that on a static site I'd need to disable TiddlyWiki's option to "link to tiddlers that do not exist yet" ($:/core/ui/ControlPanel/Settings/MissingLinks), if they're in CamelCase, because broken links lead a site to be penalized by search engines (so this is an [[SEO]] concern).

Problem is, if I disable this outright, I'd miss out on a useful core feature for when editing the wiki contents. So I was stuck with an either/or decision. Or was I?

Maybe I could temporarily disable that configuration when generating the static site! I already use a bash script to generate the site, so maybe I can just add this to the script pre-generation and re-enable it afterwards! (I never considered doing this manually, which I'm kinda too lazy to do, not to mention that this would be ripe for UserError).

The config tiddler for that feature looks like this:


```
created: 20180110215313115
modified: 20180110230252715
title: $:/config/MissingLinks
type: text/vnd.tiddlywiki

yes
```

We just need to change a small part of it to make this work (changing `yes` to `no`, and back again). But maybe we can do it even easier!

Maybe we can store the contents of the file in a variable. I figured out how to do this, [[here|https://stackoverflow.com/questions/2789319/file-content-into-unix-variable-with-newlines]].

Then we can push to that file whichever contents we want to add to it.

```bash
echo -e "created: 20180110215313115\nmodified: 20180110230252715\ntitle: $:/config/MissingLinks\ntype: text/vnd.tiddlywiki\n\nno" > 'publicwiki/tiddlers/$__config_MissingLinks.tid'

```

---

https://unix.stackexchange.com/questions/219268/how-to-add-new-lines-when-using-echo

https://www.tecmint.com/echo-command-in-linux/