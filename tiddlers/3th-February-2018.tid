created: 20180204222133543
modified: 20190309004543724
tags: Journal
title: 3th-February-2018
type: text/vnd.tiddlywiki

Scope of effect (related to effect size), number of people effected by one's efforts. Degree of effect (Total Effect) as measured by the simplified model of NumOfPeopleEffected and EffectAmountPerPerson; I suspect that there can be ways of marginally decreasing/increasing latter while increasing former to a much greater degree (Elasticity of Effect). #BizPhil #Ideas #philosophy

Mind you that the Total Effect model treats EffectPerPerson as homogenous across all people involved.

Marketing can serve as a means of boosting both factors of Total Effect.

Regarding the degree to which an idea is good or not, or will provide value that translates to revenue of the amount that you require (you need to eat, pay payroll).

---

Fixation upon notion of whether X provides value, or whether it could at a later time (delayed onset). Considered from the standpoint of WHEN it provides value, the magnitude of value is considered. But what is left unconsidered is the probability (likelihood) of you getting to that point. Many factors influence that probability, even in unintuitive ways. #Ideas #philosophy

The notion that something "scatters your force" (Emerson), diluting the degree to which you have power, or influence, or affect.

The Harry Potter principle of leadership being rightly entrusted to, if anyone, they who do not seek to have it.

The notion of power has a negative connotation surrounding it — "he is power-hungry", we say. But let's slow down, let's consider how we're using that term in those cases. There is power, one has it or hopes to have it, but I think we agree that those things alone are not what are distasteful about it; rather, we dislike that the power would enrich others and likely rob us of something. But what about when the power-full person is content? Maybe they can influence some matter to others' benefit.

Tangent: Of transactions being permissible when each party gets something from it. To get, you must be able to provide.