created: 20190220190240115
modified: 20190309004543928
tags: Economics Science Programming Education Journal Research
title: 20th-February-2019
type: text/vnd.tiddlywiki

I found a number of [[Python]]-oriented online courses and tutorials! Some of which are from the University of Helsinki.

* [[Geo-Python|https://geo-python.github.io/2018/]] (seems to be released yearly, with updates): "teaches you the basic concepts of programming using the Python programming language in a format that is easy to learn and understand (no previous programming experience required)."
* [[Automating GIS-processes|https://automating-gis-processes.github.io/2018/]] (seems to be released yearly, with updates): "course teaches you how to do different GIS-related tasks in Python programming language. Each lesson is a tutorial with specific topic(s) where the aim is to learn how to solve common GIS-related problems and tasks using Python tools. We are using only publicly available data which can be used and downloaded by anyone anywhere."
* [[Introduction to Quantitative Geology|https://introqg.github.io/qg/]]: "This course introduces students to how to study a handful of geoscientific problems using a bit of geology, math, and Python programming. The course is aimed at advanced undergraduate students in geology or geophysics."
* [[Python Testing and Continuous Integration|http://katyhuff.github.io/python-testing/]]

While looking for more courses posted online like this, I found these resources from The Carpentries (discussed further down):

* [[Plotting and Programming in Python|http://swcarpentry.github.io/python-novice-gapminder/]]

Other lessons offered by The Carpentries are [[R-programming]]-oriented.



General GIS course, too ... for some reason (a lead in to advanced lessons):

* [[Introduction to Geospatial Concepts|https://datacarpentry.org/organization-geospatial/]]

---

> The Carpentries teach foundational coding, and data science skills to researchers worldwide.

[[The Carpentries|https://carpentries.org]] seems cool (look below for links to lessons), but their website needs work.

Finding lessons for self-learning the material that they tackle isn't quite clearly discoverable --- it's hidden in the Teach nav item and from there you need to know if you want lessons regarding [[Data|http://datacarpentry.org/lessons/]], [[Software|https://software-carpentry.org/lessons/]], or [[Library|https://librarycarpentry.org/lessons/]] carpentries. It //seems// like they're not making self-learning an objective, preferring to instead funnel people into workshops (click Learn in navigation links, see items directing you to workshops).

---

Fascinating paper linked to by Software Carpentry (//"Teaching basic lab skills
for research computing"//), regarding researchers' use of Automation, (Data) Version Control, Documentation, Task Management:

Matthew Gentzkow and Jesse Shapiro: "[[Code and Data for the Social Sciences: A Practitioner's Guide|https://people.stanford.edu/gentzkow/sites/default/files/codeanddata.pdf]].", 2014.

---

I also found this lesson article that's about using [[Git]] in [[RStudio]], part of a general text about using Git: http://swcarpentry.github.io/git-novice/14-supplemental-rstudio/index.html

Programming with R: http://swcarpentry.github.io/r-novice-inflammation/ addresses a lot //except// for visualization and plotting. The lesson of which [[this article|https://datacarpentry.org/r-socialsci/04-ggplot2/index.html]] is a part might scratch the DataVisualization itch, but it doesn't seem too exhaustive.

A course on Databases and [[SQL]]: http://swcarpentry.github.io/sql-novice-survey/

* It even addresses how to access the database with [[Python|http://swcarpentry.github.io/sql-novice-survey/10-prog/index.html]] and [[R|http://swcarpentry.github.io/sql-novice-survey/11-prog-R/index.html]]

Lastly, some [[Shell scripting]] tutorials: http://swcarpentry.github.io/shell-novice/

---

!! Economics
[[Time Series Analysis for Business Forecasting|http://home.ubalt.edu/ntsbarsh/Business-stat/stat-data/Forecast.htm]]

---

It seems like the book [[Python for Data Analysis: Data Wrangling with Pandas, NumPy, and IPython|http://shop.oreilly.com/product/0636920023784.do]] from O'Reilly is well-regarded (I've seen it mentioned a lot) and its contents seem extensive.

The above was mentioned in [[this Reading List for a Fundamentals of Data Science course|https://wiki.cs.astate.edu/index.php/CS5623_Fall_2018_Reading_List]].

---

Apparently we can make books from JupyterNotebooks! See [[this repo (jupyter-book)|https://github.com/jupyter/jupyter-book]] for more. For an example, see [[the data8 book (which is quite good)|https://www.inferentialthinking.com/chapters/intro.html]].