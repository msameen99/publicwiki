#!/bin/bash
cd ~

# Check last line of config file for the toggle of ;
# if reads `yes`, print warning and proceed no further

istoggled=$(tac 'publicwiki/tiddlers/$__config_MissingLinks.tid' |egrep -m 1 .)
if [ $istoggled = "yes" ] ; then
   echo "Whoops! 'Enable links to missing tiddlers' is toggled in TiddlyWiki."
   echo "Please un-toggle 'Enable links to missing tiddlers' before running this script."
elif [ $istoggled = "no" ] ; then
   # echo "is not toggled"

    tiddlywiki publicwiki --rendertiddlers [!is[system]] $:/core/templates/staticNice.tiddler.html static text/plain
    tiddlywiki publicwiki --rendertiddler $:/blog.css static/blog.css text/plain
    tiddlywiki publicwiki --rendertiddler $:/bootstrap.min.css static/bootstrap.min.css text/plain
    tiddlywiki publicwiki --rendertiddler $:/journals-feed-atom static/journals.xml text/plain

    read -p "Site built! Want to generate images, too? (Y/n) " -n 1 -r
    echo    # (optional) move to a new line
    if [[ $REPLY =~ ^[Yy]$ ]]
    then

        tiddlywiki publicwiki --savetiddlers [is[image]] "static/images"
				tiddlywiki publicwiki --setfield [is[image]] _canonical_uri $:/core/templates/canonical-uri-external-image text/plain
				tiddlywiki publicwiki --setfield [is[image]] text "" text/plain

    fi

    sed -i 's_<hr>_<hr\/>_g' ~/publicwiki/output/static/journals.xml
    echo "Fixed the Journals feed"

    echo

    read -p "Site built! Want to open Filezilla to upload it? (Y/n) " -n 1 -r
    echo    # (optional) move to a new line
    if [[ $REPLY =~ ^[Yy]$ ]]
    then

        filezilla -c "0New site"

    fi
else
    echo "unexpected value for config of 'Enable links to missing tiddlers' in TiddlyWiki"
fi
